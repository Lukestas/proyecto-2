/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Objetos.*;
import Objetos.ObjRecintos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Lukestas
 */
public class BDParaAdmins {

    private ResultSet rs = null;
    private Statement s = null;
    BDConexion conexion = new BDConexion();
    private Connection connection = null;

    /*INICIO DE DOCENTES*/
    public ArrayList obtenerCedulasDocentes() {
        ArrayList<Integer> cedulas = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select u.cedula as cedula from usuarios u, tipos t where t.nombre_tipo='Docente' and u.id_tipo=t.id_tipo order by u.cedula asc");
            while (rs.next()) {
                cedulas.add(rs.getInt("cedula"));
            }
        } catch (Exception e) {

        }
        return cedulas;
    }

    public void IngresarDocente(ObjDocentes docente) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("insert into usuarios (cedula,nombre,id_tipo,id_genero,clave,id_jornada,especialidad) values"
                    + "(" + docente.getCedula() + ","
                    + "'" + docente.getNombre() + "', "
                    + "2,"
                    + "" + docente.getGenero() + ","
                    + "pgp_sym_encrypt('" + docente.getClave() + "','Clave'),"
                    + "" + docente.getJornada() + ","
                    + "'" + docente.getEspecialidad() + "')");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Docente agregado al registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar el registro");
        }
    }

    public ArrayList<ObjDocentes> RecuperarDocentes() {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select cedula, nombre, id_tipo,id_genero,clave,id_jornada,especialidad from usuarios where id_tipo=2 order by cedula asc");
            while (rs.next()) {
                ObjDocentes.listaDocentes.add(new ObjDocentes(rs.getInt("cedula"),
                        rs.getString("nombre"), rs.getString("clave"), rs.getInt("id_tipo"), rs.getInt("id_genero"),
                        rs.getDouble("id_jornada"), rs.getString("especialidad")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return ObjDocentes.listaDocentes;
    }

    public void EliminacionDeDocente(Integer Cedula) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("delete from usuarios where cedula=" + Cedula);
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Docente eliminado del registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la eliminación");
        }
    }

    public void ModificacionDeDocente(ObjDocentes docente, Integer Cedula) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("update usuarios set nombre='" + docente.getNombre() + "',"
                    + "cedula=" + docente.getCedula() + ","
                    + "id_jornada=" + docente.getJornada() + ","
                    + "id_genero=" + docente.getGenero() + ","
                    + "clave=pgp_sym_encrypt('" + docente.getClave() + "','Clave'),"
                    + "especialidad='" + docente.getEspecialidad() + "'"
                    + "where cedula=" + Cedula);
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Docente modificado para el registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la modificación");
        }
    }

    public ObjDocentes cargarUnDocente(Integer Cedula) {
        ObjDocentes profe = new ObjDocentes(0, "", "", 0, 0, 0, "");
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select cedula, nombre, pgp_sym_decrypt(clave::bytea,'Clave') as clave, id_tipo, id_genero, id_jornada, especialidad from usuarios where cedula=" + Cedula);
            while (rs.next()) {
                profe.setCedula(rs.getInt("cedula"));
                profe.setNombre(rs.getString("nombre"));
                profe.setClave(rs.getString("clave"));
                profe.setTipo(rs.getInt("id_tipo"));
                profe.setGenero(rs.getInt("id_genero"));
                profe.setJornada(rs.getDouble("id_jornada"));
                profe.setEspecialidad(rs.getString("especialidad"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la busqueda en el registro");
        }
        return profe;
    }

    /*FIN DE DOCENTES*/

 /*INICIO DE RECINTOS*/
    public void IngresarRecinto(ObjRecintos recinto) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("insert into recintos values(" + recinto.getCodigo() + ",'" + recinto.getNombre() + "'," + recinto.getCantAulas() + "," + recinto.getCantLabs() + ")");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Recinto agregado al registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar el registro");
        }
    }

    public ArrayList obtenerNombresRecintos() {
        ArrayList<String> Recintos = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select nombre from recintos order by nombre asc");
            while (rs.next()) {
                Recintos.add(rs.getString("nombre"));
            }
        } catch (Exception e) {

        }
        return Recintos;
    }

    public ArrayList obtenerCodigosRecintos() {
        ArrayList<Integer> Recintos = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select codigo from recintos order by codigo asc");
            while (rs.next()) {
                Recintos.add(rs.getInt("codigo"));
            }
        } catch (Exception e) {

        }
        return Recintos;
    }

    public ArrayList<ObjRecintos> RecuperarRecintos() {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo, nombre, cantidad_aulas,cantidad_laboratorios from recintos order by codigo asc");
            while (rs.next()) {
                ObjRecintos.listaRecintos.add(new ObjRecintos(rs.getInt("codigo"), rs.getString("nombre"), rs.getInt("cantidad_aulas"), rs.getInt("cantidad_laboratorios")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return ObjRecintos.listaRecintos;
    }

    public void EliminacionDeRecinto(Integer Codigo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando2 = s.executeUpdate("delete from espacios where codigo_recinto=" + Codigo);
            int comando = s.executeUpdate("delete from recintos where codigo=" + Codigo);
            if (comando == 1 && comando2 == 1) {
                JOptionPane.showMessageDialog(null, "Recinto eliminado del registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la eliminación");
        }
    }

    public void ModificacionDeRecinto(ObjRecintos recinto, Integer Codigo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando2 = s.executeUpdate("delete from espacios where codigo_recinto=" + Codigo);
            int comando = s.executeUpdate("update recintos set nombre='" + recinto.getNombre() + "',codigo=" + recinto.getCodigo() + ",cantidad_aulas=" + recinto.getCantAulas() + ",cantidad_laboratorios=" + recinto.getCantLabs() + " where codigo=" + Codigo);
            if (comando == 1 && comando2 == 1) {
                JOptionPane.showMessageDialog(null, "Recinto modificado para el registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la modificación");
        }
    }

    public ObjRecintos cargarUnRecinto(Integer Codigo) {
        ObjRecintos recinto = new ObjRecintos(0, "", 0, 0);
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo, nombre, cantidad_aulas, cantidad_laboratorios from recintos where codigo=" + Codigo);
            while (rs.next()) {
                recinto.setCodigo(rs.getInt("codigo"));
                recinto.setNombre(rs.getString("nombre"));
                recinto.setCantAulas(rs.getInt("cantidad_aulas"));
                recinto.setCantLabs(rs.getInt("cantidad_laboratorios"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la busqueda en el registro");
        }
        return recinto;
    }

    /*FIN DE RECINTOS*/
 /*INICIO DE ESPACIOS*/
    public void IngresarEspacio(ObjEspacios espacio) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("insert into espacios values('" + espacio.getCodigo() + "','" + espacio.getTipo() + "'," + espacio.getCodigo_recinto() + "," + espacio.getCupo() + ")");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Espacio agregado al registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar el registro");
        }
    }

    public String ultimaSecu() {
        String secuencia = "000";
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select secu from secuencia");
            while (rs.next()) {
                secuencia = rs.getString("secu");
            }
        } catch (Exception e) {
        }
        return secuencia;
    }

    public ArrayList obtenerCodigosEspacios() {
        ArrayList<String> Codigos = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select codigo from espacios order by codigo asc");
            while (rs.next()) {
                Codigos.add(rs.getString("codigo"));
            }
        } catch (Exception e) {
        }
        return Codigos;
    }

    public ObjEspacios cargarUnEspacio(String Codigo) {
        ObjEspacios espacio = new ObjEspacios("", "", 0, 0);
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo, tipo, codigo_recinto, cupo from espacios where codigo='" + Codigo + "'");
            while (rs.next()) {
                espacio.setCodigo(rs.getString("codigo"));
                espacio.setTipo(rs.getString("tipo"));
                espacio.setCodigo_recinto(rs.getInt("codigo_recinto"));
                espacio.setCupo(rs.getInt("cupo"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return espacio;
    }

    public Integer ObtenerCodigoRecinto(String recinto) {
        Integer codigo = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo from recintos where nombre='" + recinto + "'");
            while (rs.next()) {
                codigo = rs.getInt("codigo");
            }
        } catch (Exception e) {
        }
        return codigo;
    }

    public String ObtenerNombreRecinto(Integer Codigo) {
        String nombre = "";
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select nombre from recintos where codigo=" + Codigo);
            while (rs.next()) {
                nombre = rs.getString("nombre");
            }
        } catch (Exception e) {

        }
        return nombre;
    }

    public Integer CantLabs(Integer Codigo_Recinto) {
        Integer cantidad = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select COALESCE(count(Tipo),0)as cantidad from espacios where tipo='Laboratorio' and codigo_recinto=" + Codigo_Recinto);
            while (rs.next()) {
                cantidad = rs.getInt("cantidad");
            }
        } catch (Exception e) {

        }
        return cantidad;
    }

    public Integer CantidadLabsDeRecinto(Integer codigo) {
        Integer cantidad = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select cantidad_laboratorios from recintos where codigo=" + codigo);
            while (rs.next()) {
                cantidad = rs.getInt("cantidad_laboratorios");
            }
        } catch (Exception e) {
        }
        return cantidad;
    }

    public Integer CantAulas(Integer Codigo_Recinto) {
        Integer cantidad = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select COALESCE(count(Tipo),0)as cantidad from espacios where tipo='Aula' and codigo_recinto=" + Codigo_Recinto);
            while (rs.next()) {
                cantidad = rs.getInt("cantidad");
            }
        } catch (Exception e) {

        }
        return cantidad;
    }

    public Integer CantidadAulasDeRecinto(Integer codigo) {
        Integer cantidad = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select cantidad_aulas from recintos where codigo=" + codigo);
            while (rs.next()) {
                cantidad = rs.getInt("cantidad_aulas");
            }
        } catch (Exception e) {
        }
        return cantidad;
    }

    public ArrayList<ObjEspacios> RecuperarEspacios() {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo, tipo, codigo_recinto, cupo from espacios order by codigo asc");
            while (rs.next()) {
                ObjEspacios.listaEspacios.add(new ObjEspacios(rs.getString("codigo"), rs.getString("tipo"), rs.getInt("codigo_recinto"), rs.getInt("cupo")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return ObjEspacios.listaEspacios;
    }

    public void ActualizarSentencia(String Secuencia) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("update secuencia set secu='" + Secuencia + "'");
            if (comando == 1) {
                System.out.print("Secuencia Actualizada");
            }
        } catch (Exception e) {
        }
    }

    public void EliminacionDeEspacio(String Codigo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("delete from espacios where codigo='" + Codigo + "'");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Eliminación exitosa");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la eliminacion");
        }
    }

    public void ModificacionDeEspacio(ObjEspacios espacio, String Codigo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("update espacios set codigo='" + espacio.getCodigo() + "',tipo='" + espacio.getTipo() + "',codigo_recinto=" + espacio.getCodigo_recinto() + ",cupo=" + espacio.getCupo() + " where codigo='" + Codigo + "'");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Espacio modificado para el registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la modificación");
        }
    }

    /*FIN DE ESPACIOS*/
 /*INICIO DE CURSOS*/
    public void ModificacionDeCurso(ObjCursos curso, Integer codigo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("update cursos set codigo=" + curso.getCodigo() + ",nombre='" + curso.getNombre() + "',creditos=" + curso.getCreditos() + ",espacio='" + curso.getTipo() + "',nivel=" + curso.getNivel() + ",categoria='" + curso.getCategoria() + "' where codigo=" + codigo);
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Curso modificado para el registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la modificación");
        }
    }

    public void EliminacionDeCursos(Integer Codigo) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("delete from cursos where codigo=" + Codigo);
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Eliminación exitosa");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la eliminación");
        }
    }

    public ObjCursos cargarUnCurso(Integer Codigo) {
        ObjCursos curso = new ObjCursos(0, "", "", 0, 0, "");
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo, nombre, espacio, creditos, nivel, categoria from cursos where codigo=" + Codigo);
            while (rs.next()) {
                curso.setCodigo(rs.getInt("codigo"));
                curso.setNombre(rs.getString("nombre"));
                curso.setTipo(rs.getString("espacio"));
                curso.setCreditos(rs.getInt("creditos"));
                curso.setNivel(rs.getInt("nivel"));
                curso.setCategoria(rs.getString("categoria"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return curso;
    }

    public ArrayList<ObjCursos> RecuperarCursos() {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select codigo, nombre, espacio, creditos, nivel, categoria from cursos order by codigo asc");
            while (rs.next()) {
                ObjCursos.listaCursos.add(new ObjCursos(rs.getInt("codigo"), rs.getString("nombre"), rs.getString("espacio"), rs.getInt("creditos"), rs.getInt("nivel"), rs.getString("categoria")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return ObjCursos.listaCursos;
    }

    public void IngresarCurso(ObjCursos curso) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("insert into cursos values(" + curso.getCodigo() + ",'" + curso.getNombre() + "','" + curso.getTipo() + "'," + curso.getCreditos() + "," + curso.getNivel() + ",'" + curso.getCategoria() + "')");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "Curso agregado al registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar el registro");
        }
    }

    public Integer CantNombresCursos(String Nombre) {
        Integer cantidad = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select COALESCE(count(nombre),0)as cantidad from cursos where nombre='" + Nombre + "'");
            while (rs.next()) {
                cantidad = rs.getInt("cantidad");
            }
        } catch (Exception e) {

        }
        return cantidad;
    }

    public ArrayList obtenerCodigosCursos() {
        ArrayList<Integer> codigos = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select codigo from cursos order by codigo asc");
            while (rs.next()) {
                codigos.add(rs.getInt("codigo"));
            }
        } catch (Exception e) {
        }
        return codigos;
    }

    /*FIN DE CURSOS*/
    /*INICIO DE DISTRIBUCION*/
    public ArrayList<ObjDistribucion> RecuperarDatos(String recinto) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select recinto, espacio, franja, lunes, martes, miercoles,jueves,viernes from distribuicion where recinto='"+recinto+"'");
            while (rs.next()) {
                ObjDistribucion.listaDistribucion.add(new ObjDistribucion(rs.getString("recinto"), rs.getString("espacio"), rs.getString("franja"), rs.getString("lunes"),rs.getString("martes"), rs.getString("miercoles"), rs.getString("jueves"), rs.getString("viernes")));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar la consulta");
        }
        return ObjDistribucion.listaDistribucion;
    }
}
