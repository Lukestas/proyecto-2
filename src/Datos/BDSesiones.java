/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Objetos.ObjEstudiantes;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Lukestas
 */
public class BDSesiones {

    private ResultSet rs = null;
    private Statement s = null;
    BDConexion conexion = new BDConexion();
    private Connection connection = null;

    public String ExtraerTipo(Integer Cedula, String Contra) {
        String Tipo = "";
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("Select t.nombre_tipo as nombre_tipo from usuarios u, tipos t where t.id_tipo=u.id_tipo and u.cedula=" + Cedula + " and pgp_sym_decrypt(clave::bytea,'Clave')='" + Contra + "'");
            while (rs.next()) {
                Tipo = rs.getString("nombre_tipo");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de conexión", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return Tipo;
    }

    public String ExtraerTipo(JTextField txtCedula, JPasswordField txtClave) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void IngresarEstudiante(ObjEstudiantes estudiante) {
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            int comando = s.executeUpdate("insert into usuarios (cedula,nombre,id_tipo,fecha_nacimiento,id_genero,clave,correo) values"
                    + "(" + estudiante.getCedula() + ","
                    + "'" + estudiante.getNombre() + "', "
                    + "3,"
                    + "'" + estudiante.getFechaNac() + "',"
                    + "" + estudiante.getGenero() + ","
                    + "pgp_sym_encrypt('" + estudiante.getClave() + "','Clave'),"
                    + "'" + estudiante.getCorreo() + "')");
            if (comando == 1) {
                JOptionPane.showMessageDialog(null, "estudiante agregado al registro");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al realizar el registro");
        }
    }

    public Integer ExtraerEdad(Integer cedula) {
        Integer Edad = 0;
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select extract(year from Age(fecha_nacimiento)) as edad from usuarios where cedula=" + cedula);
            while (rs.next()) {
                Edad = rs.getInt("edad");
            }
        } catch (Exception e) {
        }
        return Edad;
    }
    
    public String ExtraerNombre(Integer cedula){
        String nombre="";
        try {
            connection=conexion.Conexion();
            s=connection.createStatement();
            rs=s.executeQuery("select nombre from usuarios where cedula="+cedula);
            while(rs.next()){
                nombre=rs.getString("nombre");
            }
        } catch (Exception e) {
        }
        return  nombre;
    }
    
    public ArrayList obtenerNombreCursos(Integer Nivel) {
        ArrayList<String> nombres = new ArrayList<>();
        try {
            connection = conexion.Conexion();
            s = connection.createStatement();
            rs = s.executeQuery("select nombre from cursos where nivel="+Nivel+" order by nombre asc");
            while (rs.next()) {
                nombres.add(rs.getString("nombre"));
            }
        } catch (Exception e) {
        }
        return nombres;
    }
    
    public void IngresarMatricula(Integer cedula,String nombre, String curso){
        try {
            connection=conexion.Conexion();
            s=connection.createStatement();
            int comando=s.executeUpdate("insert into matriculas values("+cedula+",'"+nombre+"','"+curso+"')");
            if(comando==1){
                JOptionPane.showMessageDialog(null, "Matricula realizada");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Matricula no efectuada");
        }
    }
    
}
