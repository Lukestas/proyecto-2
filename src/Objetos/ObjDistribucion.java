/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjDistribucion {
    private String Recinto;
    private String Espacio;
    private String Franja;
    private String Lunes;
    private String Martes;
    private String Miercoles;
    private String Jueves;
    private String Viernes;

    public ObjDistribucion(String Recinto, String Espacio, String Franja, String Lunes, String Martes, String Miercoles, String Jueves, String Viernes) {
        this.Recinto = Recinto;
        this.Espacio = Espacio;
        this.Franja = Franja;
        this.Lunes = Lunes;
        this.Martes = Martes;
        this.Miercoles = Miercoles;
        this.Jueves = Jueves;
        this.Viernes = Viernes;
    }
    
    public static ArrayList listaDistribucion=new ArrayList<>();

    /**
     * @return the Recinto
     */
    public String getRecinto() {
        return Recinto;
    }

    /**
     * @param Recinto the Recinto to set
     */
    public void setRecinto(String Recinto) {
        this.Recinto = Recinto;
    }

    /**
     * @return the Espacio
     */
    public String getEspacio() {
        return Espacio;
    }

    /**
     * @param Espacio the Espacio to set
     */
    public void setEspacio(String Espacio) {
        this.Espacio = Espacio;
    }

    /**
     * @return the Franja
     */
    public String getFranja() {
        return Franja;
    }

    /**
     * @param Franja the Franja to set
     */
    public void setFranja(String Franja) {
        this.Franja = Franja;
    }

    /**
     * @return the Lunes
     */
    public String getLunes() {
        return Lunes;
    }

    /**
     * @param Lunes the Lunes to set
     */
    public void setLunes(String Lunes) {
        this.Lunes = Lunes;
    }

    /**
     * @return the Martes
     */
    public String getMartes() {
        return Martes;
    }

    /**
     * @param Martes the Martes to set
     */
    public void setMartes(String Martes) {
        this.Martes = Martes;
    }

    /**
     * @return the Miercoles
     */
    public String getMiercoles() {
        return Miercoles;
    }

    /**
     * @param Miercoles the Miercoles to set
     */
    public void setMiercoles(String Miercoles) {
        this.Miercoles = Miercoles;
    }

    /**
     * @return the Jueves
     */
    public String getJueves() {
        return Jueves;
    }

    /**
     * @param Jueves the Jueves to set
     */
    public void setJueves(String Jueves) {
        this.Jueves = Jueves;
    }

    /**
     * @return the Viernes
     */
    public String getViernes() {
        return Viernes;
    }

    /**
     * @param Viernes the Viernes to set
     */
    public void setViernes(String Viernes) {
        this.Viernes = Viernes;
    }
    
}
