/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjEstudiantes {

    private Integer cedula;
    private String nombre;
    private String FechaNac;
    private Integer Genero;
    private String Clave;
    private String correo;

    public ObjEstudiantes(Integer cedula, String nombre, String FechaNac, Integer Genero, String Clave, String correo) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.FechaNac = FechaNac;
        this.Genero = Genero;
        this.Clave = Clave;
        this.correo = correo;
    }
    
    public static ArrayList listaEstudiantes=new ArrayList<>();

    /**
     * @return the cedula
     */
    public Integer getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the FechaNac
     */
    public String getFechaNac() {
        return FechaNac;
    }

    /**
     * @param FechaNac the FechaNac to set
     */
    public void setFechaNac(String FechaNac) {
        this.FechaNac = FechaNac;
    }

    /**
     * @return the Genero
     */
    public Integer getGenero() {
        return Genero;
    }

    /**
     * @param Genero the Genero to set
     */
    public void setGenero(Integer Genero) {
        this.Genero = Genero;
    }

    /**
     * @return the Clave
     */
    public String getClave() {
        return Clave;
    }

    /**
     * @param Clave the Clave to set
     */
    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

}
