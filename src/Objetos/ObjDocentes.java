/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjDocentes {
    private Integer Cedula;
    private String Nombre;
    private String Clave;
    private Integer Tipo;
    private Integer Genero;
    private double Jornada;
    private String Especialidad;

    public ObjDocentes(Integer Cedula, String Nombre, String Clave, Integer Tipo, Integer Genero, double Jornada, String Especialidad) {
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.Clave = Clave;
        this.Tipo = Tipo;
        this.Genero = Genero;
        this.Jornada = Jornada;
        this.Especialidad = Especialidad;
    }
    
    public static ArrayList listaDocentes=new ArrayList<>();

    /**
     * @return the Cedula
     */
    public Integer getCedula() {
        return Cedula;
    }

    /**
     * @param Cedula the Cedula to set
     */
    public void setCedula(Integer Cedula) {
        this.Cedula = Cedula;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Clave
     */
    public String getClave() {
        return Clave;
    }

    /**
     * @param Clave the Clave to set
     */
    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    /**
     * @return the Tipo
     */
    public Integer getTipo() {
        return Tipo;
    }

    /**
     * @param Tipo the Tipo to set
     */
    public void setTipo(Integer Tipo) {
        this.Tipo = Tipo;
    }

    /**
     * @return the Genero
     */
    public Integer getGenero() {
        return Genero;
    }

    /**
     * @param Genero the Genero to set
     */
    public void setGenero(Integer Genero) {
        this.Genero = Genero;
    }

    /**
     * @return the Jornada
     */
    public double getJornada() {
        return Jornada;
    }

    /**
     * @param Jornada the Jornada to set
     */
    public void setJornada(double Jornada) {
        this.Jornada = Jornada;
    }

    /**
     * @return the Especialidad
     */
    public String getEspecialidad() {
        return Especialidad;
    }

    /**
     * @param Especialidad the Especialidad to set
     */
    public void setEspecialidad(String Especialidad) {
        this.Especialidad = Especialidad;
    }

    /**
     * @return the listaDocentes
     */
    public static ArrayList getListaDocentes() {
        return listaDocentes;
    }

    /**
     * @param aListaDocentes the listaDocentes to set
     */
    public static void setListaDocentes(ArrayList aListaDocentes) {
        listaDocentes = aListaDocentes;
    }

        
}
