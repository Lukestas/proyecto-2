/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjProCursos {
    private Integer codigo;
    private String grupo;
    private Integer creditos;
    private String horario;
    private String espacio;
    private Integer cupo;
    private Integer cedula;
    private double jornada;

    public ObjProCursos(Integer codigo, String grupo, Integer creditos, String horario, String espacio, Integer cupo, Integer cedula, double jornada) {
        this.codigo = codigo;
        this.grupo = grupo;
        this.creditos = creditos;
        this.horario = horario;
        this.espacio = espacio;
        this.cupo = cupo;
        this.cedula = cedula;
        this.jornada = jornada;
    }
    
    
    
    public static ArrayList listaProCursos=new ArrayList<>();

    /**
     * @return the codigo
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the grupo
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    /**
     * @return the creditos
     */
    public Integer getCreditos() {
        return creditos;
    }

    /**
     * @param creditos the creditos to set
     */
    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    /**
     * @return the horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario the horario to set
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return the espacio
     */
    public String getEspacio() {
        return espacio;
    }

    /**
     * @param espacio the espacio to set
     */
    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }

    /**
     * @return the cupo
     */
    public Integer getCupo() {
        return cupo;
    }

    /**
     * @param cupo the cupo to set
     */
    public void setCupo(Integer cupo) {
        this.cupo = cupo;
    }

    /**
     * @return the cedula
     */
    public Integer getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the jornada
     */
    public double getJornada() {
        return jornada;
    }

    /**
     * @param jornada the jornada to set
     */
    public void setJornada(double jornada) {
        this.jornada = jornada;
    }
    
    
}
