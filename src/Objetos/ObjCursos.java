/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjCursos {
   private Integer Codigo;
   private String Nombre;
   private String Tipo;
   private Integer Creditos;
   private Integer Nivel;
   private String Categoria;

    public ObjCursos(Integer Codigo, String Nombre, String Tipo, Integer Creditos, Integer Nivel, String Categoria) {
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.Tipo = Tipo;
        this.Creditos = Creditos;
        this.Nivel = Nivel;
        this.Categoria = Categoria;
    }
    
    public static ArrayList listaCursos=new ArrayList<>();
    
    /**
     * @return the Codigo
     */
    public Integer getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(Integer Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Tipo
     */
    public String getTipo() {
        return Tipo;
    }

    /**
     * @param Tipo the Tipo to set
     */
    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    /**
     * @return the Creditos
     */
    public Integer getCreditos() {
        return Creditos;
    }

    /**
     * @param Creditos the Creditos to set
     */
    public void setCreditos(Integer Creditos) {
        this.Creditos = Creditos;
    }

    /**
     * @return the Nivel
     */
    public Integer getNivel() {
        return Nivel;
    }

    /**
     * @param Nivel the Nivel to set
     */
    public void setNivel(Integer Nivel) {
        this.Nivel = Nivel;
    }

    /**
     * @return the Categoria
     */
    public String getCategoria() {
        return Categoria;
    }

    /**
     * @param Categoria the Categoria to set
     */
    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }
   
}
