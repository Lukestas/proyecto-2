/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjEspacios {
    private String Codigo;
    private String Tipo;
    private Integer Codigo_recinto;
    private Integer Cupo;

    public ObjEspacios(String Codigo, String Tipo, Integer Codigo_recinto, Integer Cupo) {
        this.Codigo = Codigo;
        this.Tipo = Tipo;
        this.Codigo_recinto = Codigo_recinto;
        this.Cupo = Cupo;
    }
    
    public static ArrayList listaEspacios= new ArrayList<>();

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Tipo
     */
    public String getTipo() {
        return Tipo;
    }

    /**
     * @param Tipo the Tipo to set
     */
    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    /**
     * @return the Codigo_recinto
     */
    public Integer getCodigo_recinto() {
        return Codigo_recinto;
    }

    /**
     * @param Codigo_recinto the Codigo_recinto to set
     */
    public void setCodigo_recinto(Integer Codigo_recinto) {
        this.Codigo_recinto = Codigo_recinto;
    }

    /**
     * @return the Cupo
     */
    public Integer getCupo() {
        return Cupo;
    }

    /**
     * @param Cupo the Cupo to set
     */
    public void setCupo(Integer Cupo) {
        this.Cupo = Cupo;
    }
    
    
}
