/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjRecintos {
    private Integer Codigo;
    private String Nombre;
    private Integer CantAulas;
    private Integer CantLabs;

    public ObjRecintos(Integer Codigo, String Nombre, Integer CantAulas, Integer CantLabs) {
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.CantAulas = CantAulas;
        this.CantLabs = CantLabs;
    }
    
    public static ArrayList listaRecintos= new ArrayList<>();

    /**
     * @return the Codigo
     */
    public Integer getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(Integer Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the CantAulas
     */
    public Integer getCantAulas() {
        return CantAulas;
    }

    /**
     * @param CantAulas the CantAulas to set
     */
    public void setCantAulas(Integer CantAulas) {
        this.CantAulas = CantAulas;
    }

    /**
     * @return the CantLabs
     */
    public Integer getCantLabs() {
        return CantLabs;
    }

    /**
     * @param CantLabs the CantLabs to set
     */
    public void setCantLabs(Integer CantLabs) {
        this.CantLabs = CantLabs;
    }
    
    
}
