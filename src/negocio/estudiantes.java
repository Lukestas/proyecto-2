/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;


import Datos.BDSesiones;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Lukestas
 */
public class estudiantes {

    BDSesiones usuarios = new BDSesiones();
    
    public Integer ObtenerEdad(Integer Cedula){
        return usuarios.ExtraerEdad(Cedula);
    }
    
    public String ObtenerNombre(Integer Cedula){
        return usuarios.ExtraerNombre(Cedula);
    }
    
    public DefaultComboBoxModel cargarNombreCursos(Integer Nivel) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        ArrayList<String> Cursos = usuarios.obtenerNombreCursos(Nivel);
        modelo.addElement("Seleccione un curso");
        for (String nombres : Cursos) {
            modelo.addElement(nombres);
        }
        return modelo;
    }
    
    public void InsertarNuevaMatricula(Integer cedula, String nombre, String curso) {
        usuarios.IngresarMatricula(cedula,nombre,curso);
    }
}
