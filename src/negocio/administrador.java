/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.BDParaAdmins;
import Objetos.*;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Lukestas
 */
public class administrador {

    BDParaAdmins admins = new BDParaAdmins();

    /*INICIO DE DOCENTES*/
    public DefaultComboBoxModel cargarCedulasDocentes() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        ArrayList<Integer> Docentes = admins.obtenerCedulasDocentes();
        modelo.addElement("Seleccione un docente");
        for (Integer cedulas : Docentes) {
            modelo.addElement(cedulas);
        }
        return modelo;
    }

    public void InsertarNuevoDocente(ObjDocentes docente) {
        admins.IngresarDocente(docente);
    }

    public ArrayList<ObjDocentes> obtenerRegistrosDocentes() {
        ArrayList<ObjDocentes> Docentes = admins.RecuperarDocentes();
        return Docentes;
    }

    public void EliminarDocente(Integer Cedula) {
        admins.EliminacionDeDocente(Cedula);
    }

    public void ModificarDocente(ObjDocentes docente, Integer Cedula) {
        admins.ModificacionDeDocente(docente, Cedula);
    }

    public ObjDocentes ObtenerDocente(Integer cedula) {
        ObjDocentes datos = admins.cargarUnDocente(cedula);
        return datos;
    }

    /*FIN DE DOCENTES*/
 /*INICIO DE RECINTOS*/
    public void InsertarNuevoRecinto(ObjRecintos recinto) {
        admins.IngresarRecinto(recinto);
    }

    public DefaultComboBoxModel cargarCodigosRecintos() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        ArrayList<Integer> Recintos = admins.obtenerCodigosRecintos();
        modelo.addElement("Seleccione un recinto");
        for (Integer codigo : Recintos) {
            modelo.addElement(codigo);
        }
        return modelo;
    }

    public ArrayList<ObjRecintos> obtenerRegistrosRecintos() {
        ArrayList<ObjRecintos> Recintos = admins.RecuperarRecintos();
        return Recintos;
    }

    public void EliminarRecinto(Integer Codigo) {
        admins.EliminacionDeRecinto(Codigo);
    }

    public void ModificarRecinto(ObjRecintos recinto, Integer Codigo) {
        admins.ModificacionDeRecinto(recinto, Codigo);
    }

    public ObjRecintos ObtenerRecinto(Integer Codigo) {
        ObjRecintos recinto = admins.cargarUnRecinto(Codigo);
        return recinto;
    }

    /*FIN DE RECINTOS*/
 /*INICIO DE ESPACIOS*/
    public void ModificarEspacio(ObjEspacios espacio, String Codigo) {
        admins.ModificacionDeEspacio(espacio, Codigo);
    }

    public void EliminarEspacio(String Codigo) {
        admins.EliminacionDeEspacio(Codigo);
    }

    public void InsertarNuevoEspacio(ObjEspacios espacio) {
        admins.IngresarEspacio(espacio);
    }

    public void ActualizarLaSecuencia(String secuencia) {
        admins.ActualizarSentencia(secuencia);
    }

    public DefaultComboBoxModel cargarNombresRecintos() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        ArrayList<String> Recintos = admins.obtenerNombresRecintos();
        modelo.addElement("Seleccione un recinto");
        for (String nombre : Recintos) {
            modelo.addElement(nombre);
        }
        return modelo;
    }

    public DefaultComboBoxModel cargarCodigosEspacios() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        ArrayList<String> EspaciosC = admins.obtenerCodigosEspacios();
        modelo.addElement("Seleccione un espacio");
        for (String codigos : EspaciosC) {
            modelo.addElement(codigos);
        }
        return modelo;
    }

    public String UltimaSecuencia() {
        return admins.ultimaSecu();
    }

    public Integer CantidadLabs(Integer codigo_recinto) {
        return admins.CantLabs(codigo_recinto);
    }
    
    public Integer CantLabsRecinto(Integer codigo){
        return admins.CantidadLabsDeRecinto(codigo);
    }

    public Integer CantidadAulas(Integer codigo_recinto) {
        return admins.CantAulas(codigo_recinto);
    }
    
    public Integer CantAulasRecinto(Integer codigo){
        return admins.CantidadAulasDeRecinto(codigo);
    }

    public ObjEspacios ObtenerEspacio(String Codigo) {
        ObjEspacios espacio = admins.cargarUnEspacio(Codigo);
        return espacio;
    }

    public Integer ObtenerCodigoDelRecinto(String Nombre) {
        return admins.ObtenerCodigoRecinto(Nombre);
    }

    public String ObtenerNombreDelRecinto(Integer Codigo) {
        return admins.ObtenerNombreRecinto(Codigo);
    }

    public ArrayList<ObjEspacios> obtenerRegistrosEspacios() {
        ArrayList<ObjEspacios> Espacios = admins.RecuperarEspacios();
        return Espacios;
    }
    /*FIN DE ESPACIOS*/
 /*INICIO DE CURSOS*/
    public DefaultComboBoxModel cargarCodigosCursos(){
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        ArrayList<Integer> Cursos = admins.obtenerCodigosCursos();
        modelo.addElement("Seleccione un curso");
        for (Integer codigos : Cursos) {
            modelo.addElement(codigos);
        }
        return modelo;
    }
    
    public void InsertarNuevoCurso(ObjCursos curso) {
        admins.IngresarCurso(curso);
    }
    
    public Integer NombreExistenteCurso(String nombre){
        return admins.CantNombresCursos(nombre);
    }
    
    public ArrayList<ObjCursos> obtenerRegistrosCursos(){
        ArrayList<ObjCursos> cursos= admins.RecuperarCursos();
        return cursos;
    }
    
    public ArrayList<ObjDistribucion> obtenerRegistrosDistribuicion(String recinto){
        ArrayList<ObjDistribucion> distri= admins.RecuperarDatos(recinto);
        return distri;
    }
    
    public ObjCursos ObtenerCurso(Integer codigo){
        ObjCursos curso=admins.cargarUnCurso(codigo);
        return curso;
    }
    
    public void EliminarCursos(Integer Codigo) {
        admins.EliminacionDeCursos(Codigo);
    }
    
    public void ModificarCurso(ObjCursos curso, Integer Codigo) {
        admins.ModificacionDeCurso(curso, Codigo);
    }
    
}
